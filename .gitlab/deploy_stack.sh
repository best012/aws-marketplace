#!/usr/bin/env bash
set -euo pipefail
TRACE="${TRACE:-}"
if [[ -n "$TRACE" ]]; then
  set -x
fi

my_dir=$(dirname "$0")

# be aware this reformats and git-brands the files 
"$my_dir"/check_cfn.sh

chat_start

cd cloudformation

# Avoid doing processing on the filenames
# shellcheck disable=SC2035
../.gitlab/generate_manifest.py *.json *.template *.yaml

for f in *.json *.template *.yaml; do
    aws s3 cp "${f}" "s3://${HELM_S3_BUCKET}/${f}"
done
chat_stop
