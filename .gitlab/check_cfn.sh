#!/usr/bin/env bash
set -euo pipefail
TRACE="${TRACE:-}"
if [[ -n "$TRACE" ]]; then
  set -x
fi

if ! command -v cfn-flip &>/dev/null; then
  pip3 install cfn-flip==1.2.3
fi

chat_start

cd cloudformation

for f in *.yaml; do
  if [[ "$f" == *.iam.* ]]; then
    continue
  fi
  cfn-lint --info --include-experimental "$f"
done

# we need to distill down the main stack since with whitespace and comments
# it is bumping up against the CF TemplateBody limit (used by cfn-pivot)
# doing the VPC and RDS aren't required, but can catch weird yamlisms
# and also tag them with their git sha
for i in saas.yaml dmap.yaml stack-vpc.yaml rds.yaml; do
    bn=$(basename $i .yaml)
    sed -i"" -e '/^Description:/s/$'"/ (${CI_COMMIT_SHORT_SHA})/" $i
    cfn-flip --json < $i | jq -c . > "${bn}.template"
    cfn-flip --yaml --long         < "${bn}.template" > $i
done

chat_stop
