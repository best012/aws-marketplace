#! /usr/bin/env bash
set -euo pipefail
if [[ -n "${TRACE:-}" ]]; then
    set -x
fi
cd ami

# it seems packer no longer auto-sniffs env-vars
python3 -c '
import json
import os
import sys

exposed_envs = [
    "AMI_GROUPS",
    "AMI_EXTRA",
    "AMI_REGIONS",
    "RELEASE_ID",
]
results = {}
for e in exposed_envs:
    k = e.lower()
    v = os.getenv(e)
    if v is None:
        continue
    if e.endswith("S"):
        v = v.split(",")
    results[k] = v
json.dump(results, sys.stdout)
' > vars.auto.json
packer build -var-file=vars.auto.json "$PWD"


if [ "$PATCH_CFN_STACK" == "false" ]; then
    echo "Skipping CFN yaml patching due to \$PATCH_CFN_STACK" >&2
    exit 0
fi

patch_cfn_and_git_add() {
    local fn="$1"
    # WATCH OUT if the **assumption** that the Mappings are at the end of the file changes
    sed -i.bak -e '/^  AmiAndSnapByRegion:/,$d'  "$fn"
    # clip the "Mappings" header, but leave the indentation
    sed -e '/^Mappings:/d' stack_mapping.yaml >> "$fn"
    git add "$fn"
}

patch_cfn_and_git_add "../$CFN_STACK_FN"

git commit -m"Update with built AMIs from $CI_PIPELINE_ID"
git show --pretty=raw HEAD
  # we don't want ci.skip because we actually _do_ want it to run the normal CI
  # but it shouldn't be an infinite CI loop due to the $BUILD_AMI guard on this job
git push origin "$CI_BUILD_REF_NAME"
