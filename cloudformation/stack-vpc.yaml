AWSTemplateFormatVersion: '2010-09-09'
Description: Open Raven VPC

Outputs:
  PrivateSubnet1:
    Value: !Ref PrivateSubnet1
  PrivateSubnet1az:
    Value: !GetAtt [PrivateSubnet1, AvailabilityZone]
  PrivateSubnet2:
    Value: !Ref PrivateSubnet2
  PrivateSubnet2az:
    Value: !GetAtt [PrivateSubnet2, AvailabilityZone]
  PublicSubnet1:
    Value: !Ref PublicSubnet1
  PublicSubnet1az:
    Value: !GetAtt [PublicSubnet1, AvailabilityZone]
  PublicSubnet2:
    Value: !Ref PublicSubnet2
  PublicSubnet2az:
    Value: !GetAtt [PublicSubnet2, AvailabilityZone]
  VpcId:
    Value: !Ref OpenRavenVpc

Parameters:
  ClusterName:
    Type: String
    MinLength: 1

  OpenRavenGroupId:
    Type: String
    MinLength: 1

  OrgSlug:
    Type: String
    MinLength: 1

  VpcCidr:
    AllowedPattern: \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}/\d{1,2}
    Default: 10.128.0.0/16
    Description: the IP CIDR to assign to the VPC
    Type: String

  WithNatGw:
    Description: set to anything other than "true" to have public-only subnets
      bypassing the need for a NAT GW
    Default: 'true'
    Type: String

Conditions:
  WithNatGw: !Equals
  - Ref: WithNatGw
  - 'true'

Resources:
  OpenRavenVpc:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: !Ref VpcCidr
      EnableDnsHostnames: true
      EnableDnsSupport: true
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - orvn
          - Ref: OrgSlug
      - Key: project
        Value: openraven
      - Key: OrgSlug
        Value: !Ref OrgSlug
      - Key: OpenRavenGroupId
        Value: !Ref OpenRavenGroupId

  IGW:
    Type: AWS::EC2::InternetGateway
    Properties:
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - orvn
          - Ref: OrgSlug
      - Key: project
        Value: openraven

  IGWAttachment:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      InternetGatewayId: !Ref IGW
      VpcId: !Ref OpenRavenVpc

  PrivateSubnetS3:
    Type: AWS::EC2::VPCEndpoint
    Properties:
      ServiceName: !Sub com.amazonaws.${AWS::Region}.s3
      RouteTableIds:
      - Ref: DefaultRouteTable
      - Ref: PrivateSubnet1RT
      - Ref: PrivateSubnet2RT
      VpcId: !Ref OpenRavenVpc
      VpcEndpointType: Gateway

  DefaultRoute:
    Type: AWS::EC2::Route
    DependsOn:
    - IGWAttachment
    Properties:
      DestinationCidrBlock: '0.0.0.0/0'
      GatewayId: !Ref IGW
      RouteTableId: !Ref DefaultRouteTable

  DefaultRouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - orvn
          - Ref: OrgSlug
      - Key: project
        Value: openraven
      VpcId: !Ref OpenRavenVpc

  PrivateSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Select
      - 0
      - Fn::GetAZs:
          Ref: AWS::Region
      CidrBlock: !Select
      - 6
      # Cidr := $(ipcalc 10.128.0.0/16 --s $(
      #    aws ec2 describe-availability-zones \
      #    | jq -r '.AvailabilityZones[] | select (.State=="available") | "2048 "')
      - Fn::Cidr:
        - Ref: VpcCidr
        # number of CIDRs to generate = length(az) * 2
        # and the most AZs we have seen is in us-east-1 := 6
        - 12
        # subnet bits, so /24 = 8 bits, /20 = 12 bits
        - 12
      MapPublicIpOnLaunch: !If
      - WithNatGw
      - false
      - true
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - openraven
          - private1
          - Fn::Select:
            - 0
            - Fn::GetAZs:
                Ref: AWS::Region
      - Key: project
        Value: openraven
      - Key: !Join
        - ''
        - - kubernetes.io/cluster/
          - Ref: ClusterName
        Value: owned
      # https://github.com/kubernetes/kubernetes/blob/v1.16.2/staging/src/k8s.io/legacy-cloud-providers/aws/aws.go#L99
      - Key: kubernetes.io/role/elb
        Value: owned
      VpcId: !Ref OpenRavenVpc

  PrivateSubnet1RT:
    Type: AWS::EC2::RouteTable
    Properties:
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - orvn
          - Ref: OrgSlug
          - private1
      - Key: project
        Value: openraven
      VpcId: !Ref OpenRavenVpc

  PrivateSubnet1R:
    Type: AWS::EC2::Route
    Properties:
      DestinationCidrBlock: '0.0.0.0/0'
      GatewayId: !If
      - WithNatGw
      - Ref: AWS::NoValue
      - Ref: IGW
      NatGatewayId: !If
      - WithNatGw
      - Ref: NatGateway1
      - Ref: AWS::NoValue
      RouteTableId: !Ref PrivateSubnet1RT

  PrivateSubnet1RTA:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateSubnet1RT
      SubnetId: !Ref PrivateSubnet1

  NatEIP1:
    Condition: WithNatGw
    Type: AWS::EC2::EIP
    Properties:
      Domain: vpc
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - orvn
          - Ref: OrgSlug
          - '1'
      - Key: project
        Value: openraven

  NatGateway1:
    Condition: WithNatGw
    Type: AWS::EC2::NatGateway
    Properties:
      # watch out:
      # > Ref of an ::EIP returns the Elastic IP address.
      AllocationId: !GetAtt NatEIP1.AllocationId
      SubnetId: !Ref PublicSubnet1
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - orvn
          - Ref: OrgSlug
          - '1'
      - Key: project
        Value: openraven

  PrivateSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Select
      - 1
      - Fn::GetAZs:
          Ref: AWS::Region
      CidrBlock: !Select
      # see the PrivateSubnet1 CidrBlock for why this is 6 + 1
      - 7
      - Fn::Cidr:
        - Ref: VpcCidr
        - 12
        - 12
      MapPublicIpOnLaunch: !If
      - WithNatGw
      - false
      - true
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - openraven
          - private2
          - Fn::Select:
            - 1
            - Fn::GetAZs:
                Ref: AWS::Region
      - Key: project
        Value: openraven
      - Key: !Join
        - ''
        - - kubernetes.io/cluster/
          - Ref: ClusterName
        Value: owned
      - Key: kubernetes.io/role/elb
        Value: owned
      VpcId: !Ref OpenRavenVpc

  PrivateSubnet2RT:
    Type: AWS::EC2::RouteTable
    Properties:
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - orvn
          - Ref: OrgSlug
          - private2
      - Key: project
        Value: openraven
      VpcId: !Ref OpenRavenVpc

  PrivateSubnet2R:
    Type: AWS::EC2::Route
    Properties:
      DestinationCidrBlock: '0.0.0.0/0'
      GatewayId: !If
      - WithNatGw
      - Ref: AWS::NoValue
      - Ref: IGW
      NatGatewayId: !If
      - WithNatGw
      - Ref: NatGateway1
      - Ref: AWS::NoValue
      RouteTableId: !Ref PrivateSubnet2RT

  PrivateSubnet2RTA:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref PrivateSubnet2RT
      SubnetId: !Ref PrivateSubnet2

  PublicSubnet1:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Select
      - 0
      - Fn::GetAZs:
          Ref: AWS::Region
      CidrBlock: !Select
      - 0
      # Cidr := $(ipcalc 10.128.0.0/16 --s $(
      #    aws ec2 describe-availability-zones \
      #    | jq -r '.AvailabilityZones[] | select (.State=="available") | "2048 "')
      - Fn::Cidr:
        - Ref: VpcCidr
        # number of CIDRs to generate = length(az) * 2
        - 12
        # subnet bits, so /24 = 8 bits, /20 = 12 bits
        - 12
      # careful, if they are in the **public** subnet, they **must** have a public IP
      # since it does not use a NatGW
      MapPublicIpOnLaunch: true
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - openraven
          - public1
          - Fn::Select:
            - 0
            - Fn::GetAZs:
                Ref: AWS::Region
      - Key: project
        Value: openraven
      - Key: !Join
        - ''
        - - kubernetes.io/cluster/
          - Ref: ClusterName
        Value: owned
      # https://github.com/kubernetes/kubernetes/blob/v1.16.2/staging/src/k8s.io/legacy-cloud-providers/aws/aws.go#L99
      - Key: kubernetes.io/role/elb
        Value: owned
      VpcId: !Ref OpenRavenVpc

  PublicSubnet1RTA:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref DefaultRouteTable
      SubnetId: !Ref PublicSubnet1

  PublicSubnet2:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone: !Select
      - 1
      - Fn::GetAZs:
          Ref: AWS::Region
      CidrBlock: !Select
      - 1
      - Fn::Cidr:
        - Ref: VpcCidr
        - 12
        - 12
      MapPublicIpOnLaunch: true
      Tags:
      - Key: Name
        Value: !Join
        - '-'
        - - openraven
          - public2
          - Fn::Select:
            - 1
            - Fn::GetAZs:
                Ref: AWS::Region
      - Key: project
        Value: openraven
      - Key: !Join
        - ''
        - - kubernetes.io/cluster/
          - Ref: ClusterName
        Value: owned
      - Key: kubernetes.io/role/elb
        Value: owned
      VpcId: !Ref OpenRavenVpc

  PublicSubnet2RTA:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      RouteTableId: !Ref DefaultRouteTable
      SubnetId: !Ref PublicSubnet2
