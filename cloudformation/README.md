# What Are These Files?

* `saas.yaml` -- the CloudFormation stack that installs Open Raven 
  * `dmap.yaml` -- the sub-stack that creates the AWS resources required for DMAP on-premises
* `stack.iam.yaml` -- the IAM Policy required to run `cloudformation:CreateStack` on the `saas.yaml` template; very, very likely it is wider than it should be, so MRs are welcome 
  * `stack.iam.json` -- in some sense, this should not be in source control, since it is merely the `yaml2json` version of `stack.iam.yaml` but here we are
* `cross-account-single-policy.iam.json` -- the IAM Policy required for using the "Single Account Setup" in cross-account
* `org-readonly-role-stack.yaml` -- the CloudFormation stack that one would apply to an AWS Organizations "master account" in order to grant the Open Raven installed account the ability to run read only inspections of the AWS Organization
* `org-stackset-role-stack.yaml` -- the CloudFormation stack that one would apply to the AWS Organizations "master account" if one wished for Open Raven to manage the creation and application of StackSets assigned to the Accounts in the Organization
* `assume-role-stack-set.yaml` -- the CloudFormation stack that one would apply in a StackSet which creates the cross account IAM Role required for the Open Raven account to peer into those other member Accounts
