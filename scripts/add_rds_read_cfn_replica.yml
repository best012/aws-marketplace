- hosts: localhost
  gather_facts: false
  vars:
    ansible_python_interpreter: '{{ ansible_playbook_python }}'
  vars_prompt:
  - prompt: PgStack name?
    private: no
    name: pg_stack_name
  tasks:
  - name: obtain caller identity
    amazon.aws.aws_caller_info:
    register: my_aws

  - name: examine the current situ
    amazon.aws.cloudformation_info:
      stack_name: '{{ pg_stack_name }}'
      stack_template: yes
      stack_resources: yes
      region: us-west-2
    register: pg_cfn_info

  - name: promote RDS stack to a fact
    vars:
      pg_stack: '{{ pg_cfn_info.cloudformation.values() | first }}'
    set_fact:
      pg_stack: '{{ pg_stack }}'
      pg_stack_aws_tags: >-
        {%- set results = [] -%}
        {%- for t in pg_stack.stack_description.tags -%}
        {%-   set _ = results.append({"Key": t.key, "Value": t.value}) -%}
        {%- endfor -%}
        {{ results }}

  - name: look at the mis-named DbInst
    community.aws.rds_instance_info:
      filters:
        db-cluster-id: '{{ pg_stack.stack_resources.DbCluster }}'
      region: us-west-2
    register: db_inst_info

  - name: ensure there is only one
    assert:
      that:
      - db_inst_info.instances|length == 1
      fail_msg: It seems your DbCluster already has multiple, so fix that first

  - name: promote DbInst to a fact
    set_fact:
      db_inst: '{{ db_inst_info.instances | first }}'

  - name: create the CreateDBInstance.cli.json
    vars:
      merge_me: >-
        {{ {
          "DBClusterIdentifier": db_inst.db_cluster_identifier,
          "DBInstanceClass": db_inst.db_instance_class,
          "DBInstanceIdentifier": pg_stack.stack_resources.DbInst,
          "DBParameterGroupName": db_inst.db_parameter_groups[0].db_parameter_group_name,
          "DBSubnetGroupName": db_inst.db_subnet_group.db_subnet_group_name,
          "EngineVersion": db_inst.engine_version,
          "MonitoringRoleArn": "arn:aws:iam::%s:role/%s" % (my_aws.account, pg_stack.stack_resources.MonitoringRole),
          "Tags": pg_stack_aws_tags,
        } }}
      nope: |
          "EnableCloudwatchLogsExports": [
            "postgresql"
          ],
          "EnableIAMDatabaseAuthentication": true,
          "VpcSecurityGroupIds": [
            db_inst.vpc_security_groups[0].vpc_security_group_id,
          ],
      skeleton: |
        {
          "DBClusterIdentifier": null,
          "DBInstanceClass": null,
          "DBInstanceIdentifier": null,
          "DBParameterGroupName": null,
          "DBSubnetGroupName": null,
          "MonitoringRoleArn": null,
          "Tags": null,

          "AutoMinorVersionUpgrade": true,
          "EnablePerformanceInsights": true,
          "Engine": "aurora-postgresql",
          "LicenseModel": "postgresql-license",
          "MonitoringInterval": 5,
          "PerformanceInsightsRetentionPeriod": 7,
          "PubliclyAccessible": true
        }
    copy:
      dest: create-db-instance.cli.json
      content: '{{ (skeleton | from_json) | combine(merge_me) | to_json | string }}'
      mode: 0644

  - name: show it
    command: jq . create-db-instance.cli.json

  - name: fire it up
    when: yes_really|d(False, True)
    command: aws rds create-db-instance --cli-input-json file://create-db-instance.cli.json
