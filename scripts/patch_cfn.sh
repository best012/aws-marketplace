#! /usr/bin/env bash
set -euo pipefail
if [[ $# -eq 0 ]]; then
  # shellcheck disable=SC2016
  # shellcheck disable=SC2046
  set -- $(aws cloudformation list-stacks --stack-status-filter UPDATE_COMPLETE \
             --query 'join(`\n`, StackSummaries[*].StackName)' --output text)
  echo "I am going to patch the following stacks, because you did not specify any:" >&2
  echo "$@" >&2
  echo "Press ctrl-c if that does not look ok ..." >&2
  sleep 5
fi

for s in "$@"; do
  echo Stack $s
  tee_fn="patch-${s}.log"
  if [[ -e "$tee_fn" ]]; then
    echo "Unlikely, as \"$tee_fn\" already exists" >&2
    continue
  fi
  # The proxy setting has to do with some bug in multi processing in python
  # https://github.com/ansible/ansible/issues/65108
  no_proxy="*" ansible-playbook \
    -i localhost, \
    -v \
    -f1 \
    -e "stack_name=$s" \
    -e "ansible_python_interpreter=$(which python)" \
    ./patch_saas_cfn.yml 2>&1|tee "$tee_fn"
done
