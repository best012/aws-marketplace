source "amazon-ebs" "ami-ebs" {
  ami_description             = "Common image for an Open Raven cluster"
  ami_groups                  = var.ami_groups
  ami_name                    = "Open Raven Base Image ${var.release_id}${var.ami_extra}"
  ami_regions                 = var.ami_regions
  # required for GitLab to connect to it
  associate_public_ip_address = true
  instance_type               = var.instance_type
  launch_block_device_mappings {
    delete_on_termination = true
    device_name           = "/dev/xvda"
    volume_size           = var.volume_size
    volume_type           = "gp2"
  }
  region             = var.region
  security_group_ids = var.sg_ids
  source_ami         = var.ami_id
  ssh_username       = var.ssh_username
  subnet_id          = var.subnet_id
  vpc_id             = var.vpc_id

  tags = {
    release_id = var.release_id
    ssh_user   = var.ssh_username
  }
}

# a build block invokes sources and runs provisioning steps on them. The
# documentation for build blocks can be found here:
# https://www.packer.io/docs/from-1.5/blocks/build
build {
  name    = "openraven-base-image"
  sources = [
    "source.amazon-ebs.ami-ebs",
  ]

  provisioner "file" {
    destination = "/tmp/bootstrap-playbook.yml"
    source      = "bootstrap-playbook.yml"
  }
  provisioner "file" {
    destination = "/tmp/bootstrap-requirements.yml"
    source      = "bootstrap-requirements.yml"
  }

  provisioner "shell" {
    environment_vars = [
      "ami_extra=${var.ami_extra}",
      "release_id=${var.release_id}"
    ]
    execute_command  = "chmod +x {{ .Path }}; sudo env {{ .Vars }} {{ .Path }}"
    script           = "bootstrap0.sh"
  }

  provisioner "shell" {
    environment_vars = [
      "ami_extra=${var.ami_extra}",
      "release_id=${var.release_id}"
    ]
    # https://www.packer.io/docs/provisioners/shell#expect_disconnect
    # this is needed because the hardening script nukes SSH credentials
    # and periodically that makes packer mad
    expect_disconnect = true
    execute_command   = "chmod +x {{ .Path }}; sudo env {{ .Vars }} {{ .Path }}"
    script            = "bootstrap1.sh"
  }

  post-processor "manifest" {
    custom_data = {
      release_id = var.release_id
      ssh_user   = var.ssh_username
    }
    output = "manifest.json"
  }
  post-processor "shell-local" {
    inline = [
      "env | sort", "ls -la",
      "./manifest_to_stack_mapping.py manifest.json stack_mapping.yaml",
      "cat stack_mapping.yaml"
    ]
  }
}
