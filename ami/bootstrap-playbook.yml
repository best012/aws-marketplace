- hosts: localhost
  vars:
    ansible_python_interpreter: /opt/ansible/bin/python
  roles:
  - name: symplegma-os_bootstrap
    vars:
      bootstrap_python: no
      # supersede their package detection logic
      ansible_os_family: CoreOS
  - name: symplegma-containerd
  - name: symplegma-cni
  - name: symplegma-kubernetes_hosts

- hosts: localhost
  gather_facts: no
  vars:
    ansible_python_interpreter: /opt/ansible/bin/python

    # https://github.com/aws/amazon-ssm-agent/releases
    # - https://github.com/opsgang/amazon-ssm-agent/blob/opsgang-build-artefacts/shippable.yml
    #   - https://github.com/opsgang/amazon-ssm-agent/blob/opsgang-build-artefacts/opsgang-build-binaries.sh
    amazon_ssm_agent_feed: >-
      https://gitlab.com/openraven/open/amazon-ssm-agent-build/-/tags?format=atom

    # these are the things inside the artifacts.zip
    # and the directories where they should be unpacked
    amazon_ssm_tars:
      'binaries.tgz': /opt/amazon-ssm-agent
      'default-cfgs.tgz': /

    # https://github.com/projectcalico/calico/releases
    # and you'll want this to square up with "defaults/main.yml" in cfn-pivot
    calico_version: v3.17.1
    etcdadm:
      url: https://github.com/kubernetes-sigs/etcdadm/releases/download/v0.1.3/etcdadm-linux-amd64
      # https://github.com/kubernetes-sigs/etcdadm/releases/download/v0.1.3/etcdadm-linux-amd64-sha256
      sha256: 1cc781d15cb5994eb9918c9f6947a00481c83494e7fe86eb9aac8ffe70bdfa96

    # ideally this will line up with its helm chart
    logdna_agent_version: 2.1.9

    # https://github.com/getsentry/sentry-cli/releases
    sentry_cli_version: 1.61.0
    # they don't offer it in the rel-notes, so it needs to be cooked by hand
    sentry_cli_sha256: fb829f963e43a80c02d4fcd3fa7534a746bcccaa171df149738dc4a203213fde

    # this list is more or less:
    # k get nodes -o json \
    #   | jq -r '[.items[].status.images[].names[] | select(index("@sha256:")|not) ] | sort | unique'
    image_tags:
    - docker.io/bitnami/kafka:2.4.0-debian-9-r0
    - docker.io/bitnami/redis:5.0.8-debian-10-r10
    - docker.io/bitnami/zookeeper:3.5.6-debian-9-r27
    - docker.io/calico/cni:{{ calico_version }}
    - docker.io/calico/kube-controllers:{{ calico_version }}
    - docker.io/calico/node:{{ calico_version }}
    - docker.io/calico/pod2daemon-flexvol:{{ calico_version }}
    - docker.io/library/busybox:latest
    # this one is from etcd-prune
    - docker.io/library/python:3.8
    - docker.io/logdna/logdna-agent:{{ logdna_agent_version }}
    - gcr.io/google_containers/metrics-server-amd64:v0.3.6
    - gcr.io/datadoghq/agent:7.23.1-jmx
    - gcr.io/datadoghq/cluster-agent:1.9.1
    - k8s.gcr.io/coredns:1.6.2
    - k8s.gcr.io/defaultbackend-amd64:1.5
    - k8s.gcr.io/kube-apiserver:{{ kubernetes_version }}
    - k8s.gcr.io/kube-controller-manager:{{ kubernetes_version }}
    - k8s.gcr.io/kube-proxy:{{ kubernetes_version }}
    - k8s.gcr.io/kube-scheduler:{{ kubernetes_version }}
    - k8s.gcr.io/pause:3.1
    # CSI driver
    - k8s.gcr.io/provider-aws/aws-ebs-csi-driver:v0.9.0
    - k8s.gcr.io/sig-storage/csi-attacher:v3.0.0
    - k8s.gcr.io/sig-storage/csi-node-driver-registrar:v2.0.1
    - k8s.gcr.io/sig-storage/csi-provisioner:v2.0.2
    - k8s.gcr.io/sig-storage/csi-resizer:v1.0.0
    - k8s.gcr.io/sig-storage/csi-snapshotter:v3.0.3
    - k8s.gcr.io/sig-storage/livenessprobe:v2.2.0
    - k8s.gcr.io/sig-storage/snapshot-controller:v3.0.3
    # /CSI

    - quay.io/calico/cni:{{ calico_version }}
    - quay.io/calico/kube-controllers:{{ calico_version }}
    - quay.io/calico/node:{{ calico_version }}
    - quay.io/calico/pod2daemon-flexvol:{{ calico_version }}
    - quay.io/k8scsi/csi-attacher:v1.2.0
    - quay.io/k8scsi/csi-node-driver-registrar:v1.1.0
    - quay.io/k8scsi/csi-provisioner:v1.5.0
    - quay.io/k8scsi/csi-resizer:v0.3.0
    - quay.io/k8scsi/csi-snapshotter:v2.1.1
    - quay.io/k8scsi/livenessprobe:v1.1.0
    - quay.io/k8scsi/snapshot-controller:v2.1.1
    - quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.27.0
    - quay.io/oauth2-proxy/oauth2-proxy:v7.1.0
    - registry.gitlab.com/openraven/open/orvn-java:11

  tasks:
  - name: ensure ctr-namespace.sh
    copy:
      dest: /etc/profile.d/ctr-namespace.sh
      content: |
        CONTAINERD_NAMESPACE='k8s.io'
        export CONTAINERD_NAMESPACE
      mode: '0644'

  - name: pre-pull {{ item }}
    # redirect to eat the huge amount of ansi from their progress bar
    shell: /opt/bin/ctr -n k8s.io i pull {{ item | quote }} >/dev/null 2>&1
    # don't bother pulling all of these in vagrant
    # language=jinja2
    with_items: >-
      {{ image_tags if ansible_virtualization_type|d("") != "virtualbox" else [image_tags[0]] }}

  - name: fetch sentry-cli
    get_url:
      url: https://github.com/getsentry/sentry-cli/releases/download/{{ sentry_cli_version }}/sentry-cli-Linux-x86_64
      dest: /opt/bin/sentry-cli
      mode: '0755'
      checksum: sha256:{{ sentry_cli_sha256 }}

  - name: fetch etcdadm
    get_url:
      url: '{{ etcdadm.url }}'
      dest: /opt/bin/etcdadm
      mode: '0755'
      checksum: sha256:{{ etcdadm.sha256 }}

  - name: fetch etcdadm's etcd binary
    command: /opt/bin/etcdadm download --download-connect-timeout 2m
    args:
      # it obviously creates more than this, but good enough
      creates: /var/cache/etcdadm/etcd

  - name: fetch amazon-ssm-agent feed
    uri:
      url: '{{ amazon_ssm_agent_feed }}'
      return_content: yes
    register: ssm_feed

  - name: extract latest SSM link
    # regrettably, the "xml:" module requires lxml which is a no-go on pypy
    set_fact:
      # /feed/entry[1]/link/@href => https://gitlab.com/openraven/open/amazon-ssm-agent-build/-/tags/3.0.529.0
      feed_link: >-
        {{ ssm_feed.content | regex_findall('<link href="([^"]*)" */>') | first }}

  - name: promote link to fact
    set_fact:
      amazon_ssm_agent_zip_url: '{{ feed_link | regex_replace(re_find, re_replace) }}'
    # then, one can then obtain the build artifacts via:
    #   https://gitlab.com/openraven/open/amazon-ssm-agent-build/-/tags/3.0.529.0
    #   https://gitlab.com/openraven/open/amazon-ssm-agent-build/-/jobs/artifacts/3.0.529.0/download?job=build+binaries
    vars:
      re_find: '/tags/([0-9.]+)'
      re_replace: '/jobs/artifacts/\1/download?job=build+binaries'

  - name: fetch amazon-ssm-agent tars
    unarchive:
      src: '{{ amazon_ssm_agent_zip_url }}'
      remote_src: yes
      dest: /root
      creates: /root/binaries.tgz

  - name: ensure ssm target directories
    file:
      path: '{{ item.value }}'
      state: directory
    with_dict: '{{ amazon_ssm_tars }}'

  - name: unpack ssm artifact {{ item.key }}
    unarchive:
      src: /root/{{ item.key }}
      dest: '{{ item.value }}'
    with_dict: '{{ amazon_ssm_tars }}'

  - name: clear the ssm tars
    file:
      path: /root/{{ item.key }}
      state: absent
    with_dict: '{{ amazon_ssm_tars }}'

  - name: fix the /home/core references in amazon-ssm-agent.service
    shell: |
      if ! grep -q /home/core {{ systemd_filename }}; then
        exit 0
      fi
      sed -i"" \
        -e 's@/home/core/ssm@{{ replacement_dir }}@' \
        {{ systemd_filename }}
    vars:
      systemd_filename: /etc/systemd/system/amazon-ssm-agent.service
      replacement_dir: '{{ amazon_ssm_tars["binaries.tgz"] }}'

  - name: stop service {{ item.key }}
    service:
      name: '{{ item.key }}.service'
      state: stopped
      enabled: '{{ item.value }}'
    with_dict:
      amazon-ssm-agent: true
      containerd: true
      # kubeadm will enable it, and if we disable it here
      # then it stops chirping when the AMI boots
      kubelet: false

  - name: mask update-engine and friends
    # we need to use systemd: in order to gain access to "masked"
    # which is stronger than "enabled: no"
    systemd:
      name: '{{ item }}'
      state: stopped
      enabled: no
      masked: yes
    with_items:
    - locksmithd.service
    - update-engine.service

  - name: unset the failed status of update-engine
    # for some reason, stopping update-engine makes systemd really mad
    # and Flatcar queries the failed unit status on login, making for an ugly error
    command: systemctl reset-failed update-engine.service

  - name: write out flatcar update.conf
    # this mimicks the old cloud-config process but without involving the
    # ambiguity of "cloud-config? ignition? awesome protobuf'o'the'day?"
    # https://github.com/kinvolk/locksmith/tree/v0.6.2#configuration
    copy:
      dest: /etc/flatcar/update.conf
      content: |
        REBOOT_STRATEGY=reboot
      mode: '0644'
