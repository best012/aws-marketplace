#!/usr/bin/env bash
set -euo pipefail
ami_extra="${ami_extra:-}"
[[ -n "${ami_extra}" ]] && set -x
echo '== provision environment' >&2
set >&2
echo '==/provision environment' >&2
# WATCH OUT: these two things are tightly coupled due to the pre-built "cryptography" whl
# https://gitlab.com/openraven/open/ansible-pypy
ansible_version="${ansible_version:-2.9.13}"
ansible_whl_job=${ansible_whl_job:-752173568}

etcd_version="${etcd_version:-3.3.18}"
# yes, they want the leading "v"
kubernetes_version="${kubernetes_version:-v1.19.8}"

# sadly, there does not appear to be any stable URL and thus no shasums for it
# https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/releasehistory-aws-cfn-bootstrap.html
# but thankfully it appears to be "pip install"-able
cfn_init_url='https://s3.amazonaws.com/cloudformation-examples/aws-cfn-bootstrap-py3-2.0-2.zip'
cfn_init_venv=/opt/aws-cfn-bootstrap

ansible_whl_zip="https://gitlab.com/openraven/open/ansible-pypy/-/jobs/${ansible_whl_job}/artifacts/download"

install_cfn_bootstrap() {
    /opt/bin/pypy/bin/python -m venv ${cfn_init_venv}
    ${cfn_init_venv}/bin/pip install --upgrade setuptools wheel
    ${cfn_init_venv}/bin/pip install --upgrade pip
    ${cfn_init_venv}/bin/pip install ${cfn_init_url}
    if ${cfn_init_venv}/bin/cfn-init --help | grep -iq stack; then
      return 0
    fi
    echo 'It appears cfn-init is unwell' >&2
    return 1
}

install_ansible() {
  curl -fsSLo /root/wheels.zip "$ansible_whl_zip"
  unzip -d    /root/wheels /root/wheels.zip
  /opt/bin/pypy/bin/python -m venv --prompt ansible /opt/ansible
  /opt/ansible/bin/pip install --upgrade setuptools wheel
  /opt/ansible/bin/pip install --upgrade pip
  /opt/ansible/bin/pip install --no-index --find-links /root/wheels "ansible==${ansible_version}"
  # these appears to be safe to just pip install
  # and we need _boto_ in addition to _boto3_ due to ec2_elb using the former
  /opt/ansible/bin/pip install awscli boto boto3

  mkdir -p /etc/ansible
  # use the "ansible" python so we have access to boto3
  echo 'localhost ansible_connection=local ansible_python_interpreter=/opt/ansible/bin/python' > /etc/ansible/hosts

  rm -rf /root/wheels* /root/.cache/pip
}

install_cfn_bootstrap
install_ansible

for i in \
  /tmp/bootstrap-playbook.yml \
  /tmp/bootstrap-requirements.yml
do
  if [[ -e "$i" ]]; then
    mv -v  "$i" /root/
  fi
done

PATH=/opt/ansible/bin:$PATH
cd /root
ansible-galaxy role install -vv --keep-scm-meta --role-file bootstrap-requirements.yml
rm bootstrap-requirements.yml
ansible-playbook -vv \
  -c local \
  -i localhost, \
  -e "etcd_version=${etcd_version}" \
  -e "kubernetes_version=${kubernetes_version}" \
  -e containerd_version=1.4.4 \
  bootstrap-playbook.yml
rm bootstrap-playbook.yml

if [[ -n "${PACKER_BUILDER_TYPE:-}" ]]; then
    echo "Running ec2 security best practices script in 5 ..." >&2
    sleep 5
    # (we explicitly use $ for end-of-line, ya goofy)
    # shellcheck disable=SC2016
    curl -fsSL \
        https://github.com/awsdocs/ec2-image-builder-user-guide/raw/master/doc_source/security-best-practices.md \
      | sed -e '1,/^```/d; /^```$/d; s/ec2-user/core/g' \
      | bash -x
fi
