# aws-marketplace

The artifacts required for submission to the AWS Marketplace, which currently includes building an AMI and a CloudFormation template to consume it

# workflow

![./README.svg](./README.svg)

# How Do I Deploy This Thing For My Own `DeployChannel`?

1. Ensure your bucket is publicly readable; something akin to

   ```bash
   bucket_name=my-awesome-bucket
   aws s3api create-bucket     --bucket $bucket_name
   aws s3api put-bucket-policy --bucket $bucket_name --policy '{
       "Version": "2012-10-17",
       "Statement": [
         {
           "Effect": "Allow",
           "Principal": "*",
           "Action": "s3:GetObject",
           "Resource": "arn:aws:s3:::'${bucket_name}'/*"
         }
       ]
     }
   '
   ```

   Separately, if you wish to [deploy the `dmap` lambda jar](https://gitlab.com/openraven/open/helm-charts/helmfiles/-/blob/master/.gitlab/publish_jar_artifacts.sh#L18), you'll need to repeat that process for **every region**

   ```bash
   get_all_regions() {
       aws ec2 describe-regions --all-regions \
           --query 'join(`" "`, Regions[*].RegionName)' --output text
   }
   for r in $(get_all_regions); do run_that_script_above ${bucket_name}-${r}; done
   ```

   The provisioner needs to be able to `curl` _at least_ 3 things:
   * `saas.yaml` (well, that's AWS, but same-same)
   * `cfn-pivot.tar`
   * `helmfile.yaml`

1. Ensure it has [./cloudformation/saas.yaml](./cloudformation/saas.yaml) in it, as that is where everything begins

   From within the OpenRaven slack,

   `/gitlab aws-marketplace run publish-em my-awesome-bucket`

   will deploy **master** to your bucket, assuming the [gitlab-runner](https://console.aws.amazon.com/iam/home?region=us-west-2#/roles/gitlab-runner) has permission to write into your bucket
   
1. Ensure it has [cfn-pivot](https://gitlab.com/openraven/open/ansible-roles/cfn-pivot) in it

   From within the OpenRaven slack,
   
   `/gitlab cfn-pivot run publish-it my-awesome-bucket`
   
   will deploy **master** to your bucket, assuming the [gitlab-runner](https://console.aws.amazon.com/iam/home?region=us-west-2#/roles/gitlab-runner) has permission to write into your bucket; if you want to deploy a branch to your bucket, then [triggering CI manually](https://gitlab.com/openraven/open/ansible-roles/cfn-pivot/pipelines/new?var[HELM_S3_BUCKET]=YOUR-BUCKET-HERE) is the way to do that (although the same "able to write to your bucket" applies)

1. Ensure it has [helmfiles](https://gitlab.com/openraven/open/helm-charts/helmfiles) in it

    From within the OpenRaven slack,
    
    `/gitlab helmfiles run publish-em my-awesome-bucket`
    
    will deploy **master** to your bucket, with all of the same caveats as the item right above

1. Ensure it has [the DemoDB ES dumps](https://gitlab.com/openraven/openraven/elastic-search-node-client/-/blob/master/.gitlab/publish_es_dumps.sh) in it

   From within the OpenRaven slack,

   `/gitlab es-proxy run publish-es my-awesome-bucket`

   will deploy **master** to your bucket, with all of the same caveats as the item right above

1. Ensure it has [the monitoring charts](https://gitlab.com/openraven/openraven/monitoring-charts/-/blob/master/.gitlab/publish_helm_charts.sh)

   From within the OpenRaven slack,

   `/gitlab monitoring run publish-em my-awesome-bucket`

   will deploy **master** to your bucket, with all of the same caveats as the item right above

   
1. Fire up a CloudFormation stack

    That comes in two flavors:
    * I want to try out custom provisioning and/or helm-charts
    * I want to modify **this** repo's behavior, too
 
    For the first flow, use [the production CFN URL](https://console.aws.amazon.com/cloudformation/home#/stacks/quickcreate?templateURL=https://openraven-deploy.s3.amazonaws.com/saas.yaml&stackName=orvn-XXXX&param_OpenRavenGroupId=XXX) but **ensuring** it has your `&param_DeployChannel=my-awesome-bucket-here` in the URL (or modifying the input param on the resulting AWS console page). Also, _be sure_ to replace those `XXX` with your Okta GroupId. Using the URL that the [production website](https://www.openraven.com/install) generates is fine, just be cognizant of the `DeployChannel`
    
    For the second flow, replace the `templateURL=` in there with a URL points to `https://my-awesome-bucket-here.s3.amazonaws.com/saas.yaml` (or whatever you named `cloudformation/saas.yaml` when it was copied to your bucket)

<!--
    <div id="cfn-url">https://console.aws.amazon.com/cloudformation/home#/stacks/quickcreate?templateURL=https://openraven-deploy.s3.amazonaws.com/saas.yaml&amp;stackName=orvn-XXXX&amp;param_OpenRavenGroupId=XXX</div>
    <label>Group Id: <input type="text" name="groupId" /></label><br/>
    <label>Your Bucket: <input type="text" name="deployChannel" /></label><br/>
    <button id="buildCfn">Build My Custom URL</button>
    <script>
    document.getElementById('buildCfn').addEventListener('click', () => {
    });
    </script>
/-->
